DROP TABLE IF EXISTS Users, Books, Authors, BooksAuthors, Ratings, MarkedToRead, BookTags, Tags;
CREATE TABLE Users
(
  UserId serial NOT NULL,
  FirstName varchar(40),
  LastName varchar(40),
  Email TEXT,
  Passwd TEXT,
  PRIMARY KEY (UserId)
);

CREATE TABLE Books
(
  BookId serial NOT NULL,
  GoodReadsBookId INTEGER UNIQUE,
  Isbn TEXT,
  OriginalPublicationYear VARCHAR(7),
  OriginalTitle TEXT,
  InternationalTitle TEXT,
  LanguageCode VARCHAR(5),
  AverageRating DECIMAL,
  RatingsCount INTEGER,
  ImageUrl TEXT,
  SmallImageUrl TEXT,
  PRIMARY KEY (BookId)
);

CREATE TABLE Authors(
  AuthorId serial NOT NULL,
  FullName TEXT,
  PRIMARY KEY (AuthorId)
);

CREATE TABLE BooksAuthors(
  BookId serial NOT NULL REFERENCES books(BookId) ON DELETE RESTRICT,
  AuthorId INTEGER NOT NULL REFERENCES authors(AuthorId) ON DELETE NO ACTION,
  PRIMARY KEY (BookId, AuthorId)
);

CREATE TABLE Ratings(
  UserId INTEGER REFERENCES Users(UserId) ON DELETE NO ACTION,
  BookId INTEGER REFERENCES Books(BookId) ON DELETE NO ACTION,
  Rating VARCHAR(1),
  PRIMARY KEY (BookId, UserId)
);

CREATE TABLE MarkedToRead(
  UserId INTEGER REFERENCES Users(UserId) ON DELETE CASCADE,
  BookId INTEGER REFERENCES Books(BookId) ON DELETE CASCADE,
  PRIMARY KEY (UserId, BookId)
);

CREATE TABLE Tags(
  TagId INTEGER,
  TagName TEXT,
  PRIMARY KEY (TagId)
);

CREATE TABLE BookTags(
  BookTagsId serial NOT NULL,
  GoodReadsBookId INTEGER REFERENCES Books(GoodReadsBookId) ON DELETE CASCADE,
  TagId INTEGER REFERENCES Tags(TagId) ON DELETE CASCADE,
  Counter INTEGER,
  PRIMARY KEY (BookTagsId)
);




COPY users(FirstName, LastName, Email, Passwd) 
FROM '/home/quenar/Desktop/DLS/Project/dataset_preprocessing/netbook-dataset-preprocessing/users.csv' DELIMITER ',' CSV HEADER;

COPY books(GoodReadsBookId, Isbn, OriginalPublicationYear, OriginalTitle,
InternationalTitle, LanguageCode, AverageRating,
RatingsCount, ImageUrl, SmallImageUrl) 
FROM '/home/quenar/Desktop/DLS/Project/dataset_preprocessing/netbook-dataset-preprocessing/books_proc.csv' DELIMITER ',' CSV HEADER;

COPY authors(fullname)
FROM '/home/quenar/Desktop/DLS/Project/dataset_preprocessing/netbook-dataset-preprocessing/authors.csv' DELIMITER ',' CSV HEADER;

COPY booksauthors(bookid, authorid) 
FROM '/home/quenar/Desktop/DLS/Project/dataset_preprocessing/netbook-dataset-preprocessing/booksauthors.csv' DELIMITER ',' CSV HEADER;

COPY ratings(userid, bookid, rating) 
FROM '/home/quenar/Desktop/DLS/Project/dataset_preprocessing/netbook-dataset-preprocessing/ratings.csv' DELIMITER ',' CSV HEADER;

COPY MarkedToRead(userid, bookid) 
FROM '/home/quenar/Desktop/DLS/Project/dataset_preprocessing/netbook-dataset-preprocessing/to_read.csv' DELIMITER ',' CSV HEADER;

COPY Tags(TagId, TagName)
From '/home/quenar/Desktop/DLS/Project/dataset_preprocessing/netbook-dataset-preprocessing/tags.csv' DELIMITER ',' CSV HEADER;

COPY BookTags(GoodReadsBookId, TagId, Counter)
From '/home/quenar/Desktop/DLS/Project/dataset_preprocessing/netbook-dataset-preprocessing/book_tags.csv' DELIMITER ',' CSV HEADER;
