Howto and description.

files:
    DatasetPreprocessing.ipynb - Jupyter Notebook generating files required by 'script.sql'
    script.sql - script for recreating Postgres database for initial monotith NetBook DB
    README.dataset - readme file from dataset

Source of Current Repo:


Sources of Datasets:

    GoodReads Books Dataset:
    repo: https://github.com/zygmuntz/goodbooks-10k.git

    User Names Dataset:
    repo: https://github.com/smashew/NameDatabases.git


Creating database:

- log in to psql account
- run: 'CREATE DATABASE NOTEBOOK;'
- run: '\c notebook' to connect to database
- run: '\ir ../script.sql' where you should use relative path ie. /home/q/project/script.sql

another way:
- log in to psql account
- run: 'CREATE DATABASE NOTEBOOK;'
- '\q'
- (from console) 'psql -d notebook -f script.sql'